# Copyright (C) 2023-2025 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

.POSIX:
.SUFFIXES: # Clean-up default inference rules

# Default install directory
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

# Nothing to do
default:

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin/
	cp git-wad $(DESTDIR)$(PREFIX)/bin/
	chmod 755 $(DESTDIR)$(PREFIX)/bin/git-wad
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	cp git-wad.1 $(DESTDIR)$(MANPREFIX)/man1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/git-wad.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/git-wad
	rm -f $(DESTDIR)$(PREFIX)/share/man/man1/git-wad.1

lint:
	shellcheck -o all git-wad
	shellcheck -o all test_tiny_bin.sh
	shellcheck -o all test_update.sh
	mandoc -Wbase -Tlint git-wad.1

test:
	./test_tiny_bin.sh 1> /dev/null 2>&1
	./test_update.sh 1> /dev/null 2>&1
