#!/bin/sh

# Copyright (C) 2023-2025 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

die()
{
  rm -rf "${tmpdir}" # cleanup temporary files
  return "${1:-1}" # return status code (default is 1)
}

# Configure signal processing
trap 'die $?' EXIT
trap 'die 1' TERM INT

# Use the local git-wad
current="$(pwd)"
export PATH="${current}":"${PATH}"

# Working directory
tmpdir="$(mktemp -d "${TMPDIR:-/tmp}"/git_wad_test_XXXXXX)"
repo="${tmpdir}/update"

########################################################################
# Test setup
########################################################################
# Create the bare repository
mkdir -p "${repo}.git"
git init --bare "${repo}.git"

# Create the repository content to be managed by git-wad
mkdir -p "${repo}_ref"
dd if=/dev/random of="${repo}_ref/file0.bin" bs=1024 count="$((1*1024))"
dd if=/dev/zero   of="${repo}_ref/file1.bin" bs=1024 count="$((12*1024))"

# Use git-wad to manage binary files
printf '*.bin filter=wad' > "${repo}_ref/.gitattributes"

# Configure the directory as a git working directory, commit its
# content and push it to the remote repository
cd "${repo}_ref"
git init
git config --local user.name "Jane Doe"
git config --local user.email "jane@doe.com"
git wad init
git add ./*.bin .gitattributes
git commit -m "Commit 2 binaries"
git remote add origin "file://${repo}.git"
git push origin master
git wad push

########################################################################
# Test binary updates
########################################################################
# Clone a fresh git working directory from the remote
cd "${tmpdir}"
git clone "file://${repo}.git"

# Restore file contents and check that it the same as the original
# commited files
cd "${repo}"
git wad init
git wad pull
diff "${repo}/file0.bin" "${repo}_ref/file0.bin"
diff "${repo}/file1.bin" "${repo}_ref/file1.bin"

# Commit a new binary file from the original working directory
cd "${repo}_ref"
dd if=/dev/random of="${repo}_ref/file2.bin" bs=1024 count="314"
git add ./file2.bin
git commit -m "Add a new binary"
git push origin master
git wad push

# Pull the updates
cd "${repo}"
git pull origin master
git wad pull
git wad status
diff "${repo}/file0.bin" "${repo}_ref/file0.bin"
diff "${repo}/file1.bin" "${repo}_ref/file1.bin"
diff "${repo}/file2.bin" "${repo}_ref/file2.bin"

# Update a binary from the origin working directory
cd "${repo}_ref"
dd if=/dev/random of="${repo}_ref/file1.bin" \
  bs=1 count=123 seek="$((42*1024))" conv=notrunc
git commit -am "Update some binarie content"
git push origin master
git wad push

# Check that files differ between desynchronized working directories
cd "${repo}"
diff "${repo}/file0.bin" "${repo}_ref/file0.bin"
! diff "${repo}/file1.bin" "${repo}_ref/file1.bin" || exit 1
diff "${repo}/file2.bin" "${repo}_ref/file2.bin"
git pull origin master
git wad fetch
! diff "${repo}/file1.bin" "${repo}_ref/file1.bin" || exit 1
git wad checkout
diff "${repo}/file1.bin" "${repo}_ref/file1.bin"

########################################################################
# Test working tree clean-up
########################################################################
check_wads_count() # count
{
  n="$(find .git/wad -type f | wc -l)"
  [ "${n}" -eq "$1" ] || exit 1
}

# Check that there are 4 WADs
check_wads_count 4

# Prune it and check that nothing was removed,
# i.e. there are always 4 WADs
git wad prune
check_wads_count 4

# Prune all files except the WADs of the current HEAD, so that one file
# is deleted, namely the previous version of file1.bin before it was
# updated.
git wad prune -1
check_wads_count 3
