# Git WAD

Git filters used to manage files based on their digest.

## Requirements

- git
- POSIX shell
- curl
- rsync
- sha256sum, shasum or sha256
- [mandoc](https://mandoc.bsd.lv)

## Installation

    make install

## Test

    make test

## License

Copyright (C) 2023-2025 |Méso|Star> (contact@meso-star.com)

It is free software released under the GPL v3+ license: GNU GPL version
3 or later. You are welcome to redistribute them under certain
conditions; refer to the COPYING file for details.
