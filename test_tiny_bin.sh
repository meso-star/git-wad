#!/bin/sh

# Copyright (C) 2023-2025 |Méso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

set -e

die()
{
  rm -rf "${tmpdir}" # cleanup temporary files
  return "${1:-1}" # return status code (default is 1)
}

# Configure signal processing
trap 'die $?' EXIT
trap 'die 1' TERM INT

# Use the local git-wad
current="$(pwd)"
export PATH="${current}":"${PATH}"

# Working directory
tmpdir="$(mktemp -d "${TMPDIR:-/tmp}"/git_wad_test_XXXXXX)"
repo="${tmpdir}/tiny_bin"

########################################################################
# Test setup
########################################################################
# Create the bare repository
mkdir -p "${repo}.git"
git init --bare "${repo}.git"

# Create the repository content to be managed by git-wad
mkdir -p "${repo}_ref"
dd if=/dev/random of="${repo}_ref/file0.bin" bs=1024 count=1
dd if=/dev/random of="${repo}_ref/file1.bin" bs=1024 count=2
dd if=/dev/random of="${repo}_ref/file2.bin" bs=1024 count=3
dd if=/dev/random of="${repo}_ref/file3.bin" bs=1024 count=4
dd if=/dev/zero   of="${repo}_ref/file4.bin" bs=1024 count=5

# Add a README file to be managed by git
printf 'Repository with small binaries tracked by git-wad' \
  > "${repo}_ref/README"

# Use git-wad to manage binary files
printf '*.bin filter=wad' > "${repo}_ref/.gitattributes"

# Configure the directory as a git working directory, commit its
# content and push it to the remote repository
cd "${repo}_ref"
git init
git config --local user.name "John Doe"
git config --local user.email "john@doe.com"
git wad init
git add ./*.bin README .gitattributes
git commit -m "Test small binaries"
git remote add origin "file://${repo}.git"
git push origin master
git wad push

########################################################################
# The test
########################################################################
# Clone a fresh git working directory from the remote
cd "${tmpdir}"
git clone "file://${repo}.git"

# Check that files managed by git-wad have not yet been restored,
# whereas normal files are the same as the originals, i.e. they are not
# actually managed by git and not git-wad.
! diff "${repo}/file0.bin" "${repo}_ref/file0.bin" || exit "$?"
! diff "${repo}/file1.bin" "${repo}_ref/file1.bin" || exit "$?"
! diff "${repo}/file2.bin" "${repo}_ref/file2.bin" || exit "$?"
! diff "${repo}/file3.bin" "${repo}_ref/file3.bin" || exit "$?"
! diff "${repo}/file4.bin" "${repo}_ref/file4.bin" || exit "$?"
diff "${repo}/README" "${repo}_ref/README"
diff "${repo}/.gitattributes"  "${repo}_ref/.gitattributes"

# Restore the files managed by git-wad
cd "${repo}"
git wad init
git wad pull
git wad status

# Check that, once restored, the files managed by git-wad are the same
# as the original files.
diff "${repo}/file0.bin" "${repo}_ref/file0.bin"
diff "${repo}/file1.bin" "${repo}_ref/file1.bin"
diff "${repo}/file2.bin" "${repo}_ref/file2.bin"
diff "${repo}/file3.bin" "${repo}_ref/file3.bin"
diff "${repo}/file4.bin" "${repo}_ref/file4.bin"
diff "${repo}/README" "${repo}_ref/README"
diff "${repo}/.gitattributes"  "${repo}_ref/.gitattributes"
