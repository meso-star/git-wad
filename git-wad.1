.\" Copyright (C) 2023-2025 |Méso|Star> (contact@meso-star.com)
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd November 15, 2024
.Dt GIT-WAD 1
.Os
.Sh NAME
.Nm git-wad
.Nd manage files with git, but not their content
.Sh SYNOPSIS
.Nm
.Cm checkout
.Nm
.Cm fetch Op Fl 1a
.Nm
.Cm fsck Op Fl r
.Nm
.Cm init
.Nm
.Cm prune Op Fl 1a
.Nm
.Cm pull Op Fl 1a
.Nm
.Cm push Op Fl 1a
.Nm
.Cm status Op Fl 1a
.Sh DESCRIPTION
.Nm
allows files to be managed via git(1) without git managing their
content.
git then only archives the fingerprints of these files, here called WAD
files.
These fingerprints are used both to differentiate between different
versions of a WAD file and as pointers to its data.
The latter can be archived elsewhere than on the git repository that
manages their fingerprint, and are accessible via a potentially distinct
protocol.
.Pp
Even without tracking their content, being able to follow and
historicize files in git makes it possible to manage the consistency of
a source tree with the third-party files needed for the project, all at
once.
Third-party files whose full management by git would raise problems of
memory occupation, disk space or checking times.
.Pp
.Nm
provides a set of sub-commands for managing WAD files.
The list of files to be managed is controlled by the
.Xr gitattributes 5
file
.Po see
.Sx FILES
section
.Pc .
The subcommands are as follows:
.Bl  -tag -width Ds
.It Cm checkout
Restores WAD file data as intended by the HEAD.
It updates the contents of WAD files whose data has
.Em already
been transferred locally
.Pq see Cm fetch .
.It Cm fetch
Transfers WAD file contents locally.
The URL of the WAD repository from which data will be transferred is
defined by the
.Ev GIT_WAD_REMOTE_FETCH
environment variable.
The default setting is to transfer WAD file data for the entire HEAD
history.
.Pp
The options are as follows:
.Bl  -tag -width Ds
.It Fl 1
Transfers only WAD file data for the HEAD, not its history, i.e. it
transfers neither their previous version nor the contents of WAD files
that are no longer referenced.
.It Fl a
Transfers all versions of WAD files referenced in the complete
repository history, i.e. in all commits of all branches.
.El
.It Cm fsck
Checks locally stored WAD files.
If the
.Fl r
option is set, corrupted files are deleted.
.It Cm init
Configures the git repository for WAD file management.
Must be called in the working tree before any other
.Nm
command.
.It Cm prune
Removes non-referenced WAD file data from local storage.
By default, local data is retained for WAD files in the HEAD history.
.Pp
The options are as follows:
.Bl  -tag -width Ds
.It Fl 1
Removes all WAD file data stored locally, with the exception of WAD file
data of the HEAD.
.It Fl a
Keep local data for all versions of WAD files referenced in the complete
repository history, i.e. in all commits of all branches.
Only untracked WAD data is removed.
This is a very special case in which deleted files would have the same
naming convention as WAD data and would be stored in the WAD objects
directory without being tracked by
.Xr git 1 .
.El
.It Cm pull
Transfers locally and restores WAD file data as intended by HEAD.
This is a shortcut to the combination of the
.Cm fetch
and
.Cm checkout
subcommands.
See
.Cm fetch
for the URL of the WAD repository from which WAD data is transferred and
a list of available options.
.It Cm push
Transfers data from WAD files to the remote WAD repository.
The remote URL is defined by the
.Ev GIT_WAD_REMOTE_PUSH
environment variable.
The default setting is to transfer WAD file data for the entire HEAD
history.
.Pp
The options are as follows:
.Bl  -tag -width Ds
.It Fl 1
Transfers only WAD file data for the HEAD, not its history, i.e. it
transfers neither their previous version nor the contents of WAD files
that are no longer referenced.
.It Fl a
Transfers all versions of WAD files referenced in the complete
repository history, i.e. in all commits of all branches store locally.
.El
.It Cm status
Displays resolved, unrestored and orphaned WAD files for the current
HEAD commit.
The first are WAD files ready for use; the second are WAD
files whose data is transferred locally but whose contents have yet to
be restored
.Pq see Cm checkout ;
the third are WAD files whose data must be transferred locally before
their contents can be restored
.Pq see Cm pull .
.Pp
The initialisation status is also displayed
.Pq see Cm init ,
as well as the number of WAD file data that can be removed locally
.Pq see Cm prune .
.Pp
The options are those of the
.Cm prune
subcommand and affect the reported number of WAD file data to remove.
.El
.Sh ENVIRONMENT
.Bl -tag -width Ds
.It Ev GIT_WAD_OBJDIR
Path where WAD files are stored.
The default is the
.Pa .git/wad
subdirectory of the current working tree.
.It Ev GIT_WAD_REMOTE_FETCH
URL of the remote repository
.Em from
which WAD file data is transfered.
By default, this is the fetch URL of the
.Li origin
git repository.
.It Ev GIT_WAD_REMOTE_PUSH
URL of the remote repository
.Em to
which WAD file data is transfered.
By default, this is the push URL of the
.Li origin
git repository.
.It Ev GIT_WAD_VERBOSE
Causes
.Nm
to print debugging messages.
The default value is
.Li 0 ,
i.e. no debug messages are printed.
.El
.Sh FILES
.Bl -tag -width Ds
.It Pa .git_wad.cfg
File at the root of the working tree in which environment variables
controlling
.Nm
can be overwritten
.Po see
.Sx ENVIRONMENT
section
.Pc .
This is a shell script whose syntax must respect the POSIX shell.
Here's an example of how to define an SSH remote:
.Bd -literal -offset Ds
GIT_WAD_REMOTE_FETCH="user@remote:repo_wad"
GIT_WAD_REMOTE_PUSH="user@remote:repo_wad"
.Ed
.It Pa .gitattributes
Regular
.Xr gitattributes 5
file in which you define which files are to be managed by
.Nm .
To do this, simply add the
.Li filter=wad
attribute to these files.
For example, to manage the
.Pa test.png
image and all
pdf files with
.Nm ,
the following lines must be added to your
.Pa .gitattributes :
.Bd -literal -offset Ds
test.png filter=wad
*.pdf filter=wad
.Ed
.El
.Sh EXIT STATUS
.Ex -std
.Sh EXAMPLES
Use
.Nm
to archive pdf files of a git repository:
.Bd -literal -offset Ds
cd /path/to/git/repository
git wad init
echo "*.pdf filter=wad" >> .gitattributes
git add document.pdf
git commit -m "Add a PDF document managed by git-wad"
git push origin
git wad push
.Ed
.Pp
Clone a git repository containing WAD files:
.Bd -literal -offset Ds
git clone user@remote:repo.git
cd repo
git wad init
git wad pull
.Ed
.Pp
Same as above, but saves disk space by transferring only the contents of
WAD files referenced by HEAD:
.Bd -literal -offset Ds
git clone user@remote:repo.git
cd repo
git wad init
git wad pull -1
.Ed
.Pp
Synchronise the local working tree with the state of the
.Ar origin
repository and update the contents of WAD files accordingly:
.Bd -literal -offset Ds
git pull origin
git wad pull
.Ed
.Pp
Check locally stored WAD files and delete corrupted ones before
retransferring missing files:
.Bd -literal -offset Ds
git wad fsck -r
git wad pull
.Ed
.Pp
Make space on disk by deleting the contents of WAD files that are not
referenced by HEAD:
.Bd -literal -offset Ds
git wad prune -1
.Ed
.Pp
Transfer all locally stored WAD files to another remote repository:
.Bd -literal -offset Ds
GIT_WAD_REMOTE_PUSH="user@another_remote:repo_wad" \e
git wad push -a
.Ed
.Sh SEE ALSO
.Xr git 1 ,
.Xr gitattributes 5
